<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $table = "Movies";
    protected $primaryKey = "idmovie";
    public $timestamp = false;
    protected $fillable = ["name", "description"];
    protected $guard = ["idmovie"];
}
