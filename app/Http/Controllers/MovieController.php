<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;
use Illuminate\Support\Facades\Redis;

class MovieController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $data)
    {
        if (isset($data->search)) {
            $data = Movie::where("name", "like", "%" . $data->search . "%")->get();
            return view('movie.list', ['movies' => $data]);
        } else {
            return view('movie.list', ['movies' => Movie::all()]);
        }
    }
    public function store(Request $data)
    {
        $this->validate($data, [
            "name" => "required|max:100",
            "description" => "required"
        ]);
        $movie = new Movie();
        $movie->name = $data->name;
        $movie->description = $data->description;
        if ($movie->save()) {
            return redirect()->route("movie.create")->with('msgOk', 'Pelicula almacenada correctamente');
        } else {
            return redirect()->route("movie.create")->with("msgBad", "Error al almacenar la pelicula");
        }
    }
    public function create()
    {
        return view('movie.create');
    }
    public function edit(Request $data)
    {
        return view("movie.edit", ["movie" => Movie::find($data->id)]);
    }
    public function update(Request $data)
    {
        $this->validate($data, [
            "name" => "required|max:100",
            "description" => "required"
        ]);
        $movie = Movie::find($data->id);
        $movie->name = $data->name;
        $movie->description = $data->description;
        if ($movie->save()) {
            return redirect()->route("movie.index")->with('msgOk', 'Pelicula actualizada correctamente');
        } else {
            return redirect()->route("movie.index")->with("msgBad", "Error al actualizar la pelicula");
        }
    }
    public function delete(Request $data)
    {
        if (Movie::destroy($data->id)) {
            return redirect()->route("movie.index")->with('msgOk', 'Pelicula eliminada correctamente');
        } else {
            return redirect()->route("movie.index")->with("msgBad", "Error al eliminar la pelicula");
        }
    }
}
