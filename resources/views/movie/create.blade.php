@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <a href="{{ route("movie.index") }}" class="btn btn-info mb-3 text-white">Ver Listado</a>
            <form action="{{ route("movie.store") }}" method="POST">
                @csrf
                <div class="form-group">
                  <label for="name">Nombre</label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Nombre">
                </div>
                <div class="form-group">
                  <label for="description">Descripcion</label>
                  <textarea name="description" class="form-control" id="description" placeholder="Descripcion" cols="20" rows="8"></textarea>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Almacenar Pelicula</button>
              </form>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
  @endsection