@extends('layouts.app')
@section('content')
<div class="container">
    <a href="{{ route("movie.create") }}" class="btn btn-block btn-success mb-3">Agregar pelicula</a>
    @include("movie.formSearch")
    @if(!empty($movies))
    <div class="row row-cols-1 row-cols-md-3">
        @foreach ($movies as $movie)
        <div class="col mb-4">
            <div class="card">
                <div class="card-body">
                <h5 class="card-title">{{ $movie->name }}</h5>
                <p class="card-text">{{  $movie->description }}</p>
                </div>
                <div class="card-footer d-inline">
                    <a href="{{ route("movie.edit",["id"=>$movie->idmovie]) }}" class="btn btn-outline-primary">Actualizar</a>
                    <form action="{{ route("movie.delete") }}" method="POST" class="float-right prevent-delete">
                     <input type="hidden" name="id" value="{{$movie->idmovie}}">
                        @csrf
                        @method("DELETE")
                        <input type="submit" class="btn btn-outline-danger" value="Eliminar">
                    </form>
                </div>
            </div>
        </div>
            @endforeach
        </div>
        {{-- {{ $movies->links() }} --}}
    @else
    <h2 class="text-center">No hay Peiculas almacenadas</h2>
      @endif
    </div>
@endsection