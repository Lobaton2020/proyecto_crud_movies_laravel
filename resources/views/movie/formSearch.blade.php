
<form action="{{route("movie.index")}}" >
    <div class="row my-2">
        <div class="col-md-9">
            <input type="text" class="form-control" name="search" class="form-control" placeholder="Buscar pelicula">
        </div>
        <div class="col-md-1">
            <button type="submit" class="btn btn-outline-primary">Buscar</button>
        </div>
        <div class="col-md-2">
            <a href="{{ route("movie.index") }}" class="btn btn-info text-white">Ver todo</a>
        </div>
    </div>
</form>