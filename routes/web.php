<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/movies', 'MovieController@index')->name('movie.index');
Route::get('/movies/create', 'MovieController@create')->name('movie.create');
Route::post('/movies', 'MovieController@store')->name('movie.store');
Route::get('/movies/{id}/edit', 'MovieController@edit')->name('movie.edit');
Route::put('/movies', 'MovieController@update')->name('movie.update');
Route::delete('/movies', 'MovieController@delete')->name('movie.delete');
