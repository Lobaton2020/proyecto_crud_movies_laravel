((window, document) => {

    const handlerPreventDelete = (e) => {
        e.preventDefault();
        if (confirm("Estas seguro de eliminar este registro?")) {
            e.target.submit();
        }
    };
    const initDOM = (e) => {
        document.querySelectorAll(".prevent-delete")
            .forEach((elem) => {
                console.log(1)
                elem.addEventListener("submit", handlerPreventDelete);
            });
    };

    document.addEventListener("DOMContentLoaded", initDOM)
})(window, document);